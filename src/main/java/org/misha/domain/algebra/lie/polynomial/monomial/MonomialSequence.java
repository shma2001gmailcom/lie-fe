package org.misha.domain.algebra.lie.polynomial.monomial;

import org.apache.log4j.Logger;
import org.misha.repository.MonomialService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Author: mshevelin
 * Date: 11/27/14
 * Time: 1:13 PM
 */

class MonomialSequence {
    private static final Logger log = Logger.getLogger(MonomialSequence.class);
    private static final int threshold = 5000;
    private final Set<Monomial> sequence = new HashSet<>(2 * threshold);
    private final Monomial lastMonomial;


    MonomialSequence(final String... rawAlphabet) {
        Monomial mLetter = null;
        for (final String letter : rawAlphabet) {
            if (letter.length() == 1) {
                mLetter = MonomialUtils.monomial(letter);
                sequence.add(mLetter);
            }
        }
        lastMonomial = mLetter;
    }

    private MonomialSequence(final Monomial... alphabet) {
        Monomial currentLetter = null;
        for (final Monomial letter : alphabet) {
            sequence.add(letter);
            currentLetter = letter;
        }
        lastMonomial = currentLetter;
    }

    Monomial getNextMonomial(final Monomial m) {
        if (!m.isCorrect()) {
            throw new IllegalArgumentException("The monomial '" + m + "' must be correct.");
        }
        Monomial product;
        final Set<Monomial> lefts = new TreeSet<>(sequence);
        final Set<Monomial> rights = new TreeSet<>(sequence);
        for (final Monomial left : lefts) {
            for (final Monomial right : rights) {
                product = MonomialUtils.monomial(left, right);
                if (product.isCorrect() && product.compareTo(m) > 0) {
                    sequence.add(product);
                    return product;
                }
            }
        }
        return null;
    }

    static boolean isCorrect(Monomial left, Monomial right) {
        if (left.isLetter()) return right.isLetter() && left.compareTo(right) > 0;
        return left.compareTo(right) > 0 && left.right().compareTo(right) <= 0;
    }

    private void getNextDbMonomial(final MonomialService monomialService) {
        Monomial product;
        int i = 0;
        while (i < threshold) {
            final Set<Monomial> lefts = new HashSet<>(sequence);
            final Set<Monomial> rights = new HashSet<>(sequence);
            for (final Monomial left : lefts) {
                for (final Monomial right : rights) {
                    if (isCorrect(left, right)) {
                        product = MonomialUtils.monomial(left, right);
                        if (sequence.add(product)) {
                            ++i;
                            product.setId(monomialService.write(product));
                        }
                    }
                }
            }
            log.info(i);
        }
    }

    Monomial getLastMonomial() {
        return lastMonomial.copy();
    }

    public static void main(String... args) throws InterruptedException {
        final BeanFactory factory = new ClassPathXmlApplicationContext("applicationContext.xml");
        final MonomialService monomialService = (MonomialService) factory.getBean("monomialService");
        final Monomial[] alphabet = new Monomial[3];
        for (int i = 0; i < 3; ++i) {
            alphabet[i] = monomialService.findBySmallId((long) i + 1);
        }
        MonomialSequence monomialSequence = new MonomialSequence(alphabet);
        monomialSequence.getNextDbMonomial(monomialService);
    }
}
