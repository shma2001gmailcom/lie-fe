package org.misha.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.inject.Named;
import javax.sql.DataSource;

@Configuration
public class Config {

    @Named("dataSource")
    @Primary
    @Bean
    public DataSource dataSource() {
        return new DataSourceFactory().getDataSource();
    }
}
