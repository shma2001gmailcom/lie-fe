package org.misha.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;


public class DataSourceFactory {
    private final HikariDataSource ds;

    public DataSourceFactory() {
        ds = new HikariDataSource(new HikariConfig("jdbc.properties"));
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public DataSource getDataSource() {
        return ds;
    }

}
