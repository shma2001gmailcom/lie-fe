--################################################################
--#################### postgresql ################################
--################################################################

drop table if exists nodes;
drop table if exists node_data;
drop table if exists polynomials;


create table if not exists polynomials (
  polynomial_id serial4 primary key not null unique,
  monomial_id   bigint,
  scalar        int,
  is_final      boolean
);

create index polynomials_pol_id_mon_id on polynomials (polynomial_id, monomial_id);

create table if not exists nodes (
  node_id  bigint,
  left_id  bigint,
  right_id bigint,
  data_id  bigint
);

create table if not exists node_data (
  node_id serial4 primary key not null unique,
  data_value varchar(200) unique
);

create or replace procedure new_node_letter(in node_data varchar(200))
  as $$
  declare next_id bigint default null;
  begin
    insert into node_data(data_value) values (node_data) on conflict do nothing returning node_id into next_id;
    insert into nodes(left_id, right_id, data_id) values (null, null, next_id);
  end;
$$ language plpgsql;

create or replace procedure alphabet()
  as $$
  begin
	call new_node_letter('a');
	call new_node_letter('b');
	call new_node_letter('c');
  end;
$$ language plpgsql;

create or replace function new_node(lleft_id bigint, rright_id bigint)
  returns bigint
  as $$
    declare left_data varchar(200) default null;
    right_data varchar(200) default null;
    to_insert varchar(200) default null;
    next_id bigint default null;
  begin
    select data_value
    into left_data
    from node_data
    where node_id = lleft_id;
    select data_value
    into right_data
    from node_data
    where node_id = rright_id;
    to_insert = concat(concat(concat(concat('[', left_data), ', '), right_data), ']');
    insert into node_data(data_value) values (to_insert) on conflict do nothing returning node_id into next_id;
    insert into nodes(left_id, right_id, data_id) values (lleft_id, rright_id, next_id);
    return next_id;
  end;
$$ language plpgsql;

create or replace function new_polynomial()
  returns bigint
  as $$
    declare polynomial_count bigint default 0;
  begin
    select count(1)
    from polynomials
    into polynomial_count;
    insert into polynomials values (polynomial_count + 1, null, 0, false);
    return polynomial_count + 1;
  end;
$$ language plpgsql;

create or replace procedure update_polynomial(in in_polynomial_id bigint, in in_monomial_id bigint, in in_scalar int)
  as $$
    declare current_monomial_id bigint;
    current_scalar int;
    is_done boolean default false;
    is_updated boolean default false;
    curs cursor for (
      select
        polynomials.monomial_id,
        polynomials.scalar
      from polynomials
      where polynomial_id = in_polynomial_id);
  begin
    open curs;
    while not is_done loop
      fetch curs
      into current_monomial_id, current_scalar;
      if not found then is_done := true;
      end if;
      if current_monomial_id is null
      then --first update after creation
        is_done = true;
        is_updated = true;
        update polynomials
        set scalar = in_scalar, monomial_id = in_monomial_id
        where polynomial_id = in_polynomial_id;
      elseif current_monomial_id = in_monomial_id
        then --collect similar terms
          is_done := true;
          is_updated := true;
          update polynomials
          set scalar = (in_scalar + current_scalar)
          where polynomial_id = in_polynomial_id and monomial_id = in_monomial_id;
      end if;
    end loop;
    if not is_updated
    then
      insert into polynomials values (in_polynomial_id, in_monomial_id, in_scalar, false);
    end if;
    close curs;
  end;
$$ language plpgsql;

drop procedure if exists finalize_polynomial;

create or replace procedure finalize_polynomial(in in_polynomial_id bigint)
  as $$
  begin
    update polynomials
    set is_final = true
    where polynomial_id = in_polynomial_id;
  end;
$$ language plpgsql;

--########################### test ########################################
call alphabet();

select
  n.node_id,
  n.left_id,
  n.right_id,
  d.data_value
from nodes n left join node_data d on (n.node_id = d.node_id)
where right_id = 3
order by n.node_id;

select new_polynomial();

select * from polynomials;